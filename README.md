# TextGrid Repository and API Documentation

This repository gathers all documentation of TG-rep services and deploys them as the TextGrid Repository Documentation to <https://textgridlab.org/doc>, <https://test.textgridlab.org/doc>, or <https://dev.textgridlab.org/doc>.

Please initially do a:

    git submodule init

and then each time to update modules:

    git submodule update --force --remote

For every push into the develop branch a SNAPSHOT build is being performed (and deployed to dev.textgridlab.org), the main branch is build as a RELEASE build (and deployed to textgridlab.org). For more information please see RELEASE.md.

If you want to add a new submodule, please use:

    git submodule add [git repo of module] [folder of submodule]

such as

    git submodule add git@gitlab.gwdg.de:dariah-de/dariah-de-iiif-metadata.git docs/submodules/dariah-de-iiif-metadata

Also you could have a look into our .gitlab-ci.yml file, where all the cool workflow stuff is configured.

## Release of TextGrid Repository Documentation

Please just commit and push to develop and then use the [DARIAH-DE Release Workflow](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management).

No versions have to be changed, all versioning is done in gitlab CI/CD. You *can* adapt apt package names in gitlab-ci, but you *do not have to*!
