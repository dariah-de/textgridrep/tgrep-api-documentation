Citation of TextGrid Objects
============================

Our citation suggestions for TextGrid objects are roughly based on the `Datacite Citation Examples <https://datacite.org/cite-your-data.html>`_. You can cite TextGrid objects as follows based on the following schema, tags are referring to the `Textgrid Metadata Schema <http://textgrid.info/namespaces/metadata/core/2010>`_. The following suggestions are automatically build from the object's metadata and are shown in `textgridrep.org <https://textgridrep.org>`_ (https://textgridrep.org/browse/-/browse/11dpj_0#citation).


Editions
--------

::

    TextGrid Repository (<issued>). <author>. <title>. <project>. <resolver/hdl>


1. TextGrid Repository (2012). Spyri, Johanna. Heidi kann brauchen, was es gelernt hat. Digitale Bibliothek. https://hdl.handle.net/11858/00-1734-0000-0005-1422-F

2. TextGrid Repository (2017). Virtuelles Skriptorium St. Matthias (www.stmatthias.uni-trier.de). Bibliothek des Bischöflichen Priesterseminars, Hs. 88: Sammelhandschrift. Virtuelles Skriptorium St. Matthias. https://hdl.handle.net/11378/0000-0006-BA87-0


Collections
-----------

::

    TextGrid Repository (<issued>). <title>. <project>. <collector>. <resolver/hdl>


1. TextGrid Repository (2012). Spyri, Johanna. Digitale Bibliothek. Editura. https://hdl.handle.net/11858/00-1734-0000-0005-141F-A

2. TextGrid Repository (2016). Notizbuch C09 JPEG Images. Fontane Notizbücher. Gabriele Radecke. https://hdl.handle.net/11378/0000-0005-F649-4


Aggregations
------------

::

    TextGrid Repository (<issued>). <path>. <title>. <project>. <rightsHolder>. <resolver/hdl>


1. TextGrid Repository (2012). Nachtigal, Johann Carl Christoph. Sagen. Volcks-Sagen. 2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes. 24. Ueber die Hühnen- und Zwerg-Sagen. Digitale Bibliothek. TextGrid. https://hdl.handle.net/11858/00-1734-0000-0004-5E04-3

2. TextGrid Repository (2017). Bibliothek des Bischöflichen Priesterseminars, Hs. 6: Handschrift für den Chor in Schablonentechnik. Inhalte. Bibliothek des Bischöflichen Priesterseminars Trier Hs. 6. Antiphonale Romano-monasticum. Proprium antiphonarum de tempore. Seite 31. Virtuelles Skriptorium St. Matthias. Bibliothek des Bischöflichen Priesterseminars Trier. https://hdl.handle.net/11378/0000-0006-1EA8-C


Works
-----

::

    TextGrid Repository (<issued>). <author>. <title>. <project>. <resolver/hdl>


1. TextGrid Repository (2012). Egenolff, Christian. Ein muck füret es auff dem schwantz über Rhein. Digitale Bibliothek. https://hdl.handle.net/11858/00-1734-0000-0002-8ACA-C

2. TextGrid Repository (2012). Tucholsky, Kurt. Krach!. Digitale Bibliothek. https://hdl.handle.net/11858/00-1734-0000-0005-6595-6


Items
-----

::

    TextGrid Repository (<issued>). <path>. <title>. <project>. <rightsHolder>. <resolver/hdl>


1. TextGrid Repository (2012). Spyri, Johanna. Biographie: Spyri, Johanna. Johanna Spyri (Fotografie von Johannes Ganz in Zürich, um 1875). Digitale Bibliothek. TextGrid. https://hdl.handle.net/11858/00-1734-0000-0005-142A-0

2. TextGrid Repository (2017). Bibliothek des Bischöflichen Priesterseminars, Hs. 58: Sammelhandschrift. Inhalte. Bibliothek des Bischöflichen Priesterseminars Trier Hs. 58. Sermones in festivitatibus Domini et b. Virginis. Blatt 231v. Blatt 231v. Virtuelles Skriptorium St. Matthias. Bibliothek des Bischöflichen Priesterseminars Trier. https://hdl.handle.net/11378/0000-0006-7326-E
