.. TextGrid Repository documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


The TextGrid Repository Documentation
=====================================

The `TextGrid Repository <https://textgridrep.org>`__ is a long-term archive for humanities research data. It provides an extensive, searchable, and reusable repository of texts and images. Aligned with the principles of Open Access and FAIR, the TextGrid Repository was awarded the `CoreTrustSeal <https://www.coretrustseal.org/why-certification/certified-repositories/>`__ in 2020. For researchers, the TextGrid Repository offers a sustainable, durable, and secure way to publish their research data in a citable manner and to describe it in an understandable way through required metadata. Read more about sustainability, FAIR and Open Access in the `Mission Statement of the TextGrid Repository <https://textgridrep.org/docs/mission-statement>`__.

On this site you can find information on organizational infrastructure, data policies, and digital object management of the DARIAH-DE Repository, user guides and public APIs are documented as well. They provide information on access, import, dissemination, and resolving. You can find more information about the TextGrid Repository on the `TextGrid Website <https://textgrid.de>`__.


.. toctree::
  :maxdepth: 2
  :caption: TextGrid Repository

  ./organizational-infrastructure
  ./data-policies
  ./digital-object-management


.. toctree::
  :maxdepth: 2
  :caption: Core Services

  ./submodules/tg-auth/docs/index
  ./submodules/tg-crud/tgcrud-webapp/docs/index
  ./submodules/tg-search/docs/index


.. toctree::
  :maxdepth: 2
  :caption: Import and Dissemination

  ./submodules/kolibri/kolibri-addon-textgrid-import/docs/index
  ./submodules/aggregator/docs/index
  ./submodules/oai-pmh/docs_tgrep/index
  ./submodules/tg-digilib/docs_tgrep/index
  ./submodules/tg-iiif-metadata/docs_tgrep/index
  ./triplestore


.. toctree::
  :maxdepth: 2
  :caption: Extended APIs

  ./submodules/tg-pid/docs_tgrep/index
  ./submodules/kolibri/kolibri-tgpublish-service/docs/index


.. toctree::
  :maxdepth: 2
  :caption: TextGrid Service Clients

  ./submodules/textgrid-java-clients/docs/index
  TextGrid Python Clients <https://dariah-de.pages.gwdg.de/textgridrep/textgrid-python-clients/docs/index.html>


.. toctree::
  :maxdepth: 2
  :caption: Resolving

  ./resolver


.. toctree::
  :maxdepth: 2
  :caption: Citation

  ./citation
