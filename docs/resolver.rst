Resolving TextGrid URIs and PIDs
================================


TextGrid URIs
-------------
A TextGrid URI is associated with a single TextGrid object and looks like

::

	textgrid:vqn2.0

TextGrid URIs are persistent IDs in the TextGrid scope and are used to access the different parts of a TextGrid Object: The data part, the metadata part (descriptive metadata), and the technical metadata part.

Public TextGrid Objects
^^^^^^^^^^^^^^^^^^^^^^^
Every public TextGrid object can be accessed directly by the TG-crud service by everyone. The main URL to access the object (for HTTP RESTful API use) would be the resource:

::

  https://textgridlab.org/1.0/tgcrud/rest/textgrid:vqn2.0

The three parts of the textGrid object can be accessed as follows (data, descriptive metadata, and technical metadata):

* https://textgridlab.org/1.0/tgcrud/rest/textgrid:vqn2.0/data
* https://textgridlab.org/1.0/tgcrud/rest/textgrid:vqn2.0/metadata
* https://textgridlab.org/1.0/tgcrud/rest/textgrid:vqn2.0/tech

Non-Public TextGrid Objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^
TextGrid objects that are not yet published (non-public) can also be accessed adding the RBAC Session ID (see TextGridLab Authentication) as a parameter as following:

* https://textgridlab.org/1.0/tgcrud/rest/textgrid:vqn2.0/data?sessionId=12345678ABCDEFGH
* https://textgridlab.org/1.0/tgcrud/rest/textgrid:vqn2.0/metadata?sessionId=12345678ABCDEFGH

Technical metadata are extracted automatically during the publishing process, so no technical metadata are available for non-public TextGrid objects.


Persistent Identifiers
----------------------
Each published Textgrid object gets an ePIC Handle PID during the publishing process that looks like:

::

	hdl:11858/00-1734-0000-0005-1424-B

Publicly available TextGrid Objects can so be addressed via their PID and a PID resolver, such as https://hdl.handle.net or https://doi.org. The TextGrid object's PID you can find in the metadata of every public TextGrid object in the

::

	<pid pidType="handle">hdl:11858/00-1734-0000-0005-1424-B</pid>

tag and can be accessed using every Handle Resolver Service (the "hdl:" prefix may be omitted here):

* https://hdl.handle.net/11858/00-1734-0000-0005-1424-B
* https://doi.org/11858/00-1734-0000-0005-1424-B

Each PID is resolved directly to its TextGrid URI and resolver. You can also access every published object with its Handle PID using TG-crud:

* https://textgridlab.org/1.0/tgcrud/rest/hdl:11858/00-1734-0000-0005-1424-B/data
* https://textgridlab.org/1.0/tgcrud/rest/hdl:11858/00-1734-0000-0005-1424-B/metadata
* https://textgridlab.org/1.0/tgcrud/rest/hdl:11858/00-1734-0000-0005-1424-B/tech


Resolving
---------

Resolving of TextGrid URIs
^^^^^^^^^^^^^^^^^^^^^^^^^^
Every TextGrid object can be addressed via the Textgrid Resolver *textgridrep.org* and its TextGrid URI. The resolver will always point to the correct TG-crud instance:

* https://textgridrep.org/textgrid:vqn2.0/data
* https://textgridrep.org/textgrid:vqn2.0/metadata
* https://textgridrep.org/textgrid:vqn2.0/tech (only public objects)

Non-public TextGrid objects can be resolved via:

* https://textgridrep.org/textgrid:vqn2.0/data?sessionId=12345678ABCDEFGH
* https://textgridrep.org/textgrid:vqn2.0/metadata?sessionId=12345678ABCDEFGH

Resolving of TextGrid ePIC Handle PIDs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Every public TextGrid object can also be addressed via the Textgrid Resolver *textgridrep.org* and its TextGrid Handle PID. The resolver will always point to the correct TG-crud instance (the hdl: namespace prefix may NOT be omitted here!):

* https://textgridrep.org/hdl:11858/00-1734-0000-0005-1424-B/data
* https://textgridrep.org/hdl:11858/00-1734-0000-0005-1424-B/metadata
* https://textgridrep.org/hdl:11858/00-1734-0000-0005-1424-B/tech


Content Negotiation
===================

Both the URLs

* https://textgridrep.org/textgrid:vqn2.0

and

* https://textgridrep.org/hdl:11858/00-1734-0000-0005-1424-B

are resolving **(i)** to the HTML representation of the object (the TextGrid Repository Portal) if accessed via browser (and/or HTTP Acceppt header: text/html):

* https://textgridrep.org/browse/vqn2.0

and **(ii)** to the data object directly if accessed otherwise:

* https://textgridlab.org/1.0/tgcrud/rest/textgrid:vqn2.0/data
