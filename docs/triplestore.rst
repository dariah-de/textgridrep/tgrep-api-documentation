Triplestore Queries
===================

The SPARQL search API for the TextGrid triplestore is publicly available. As the triplstore contains only structural data without any title information, it is possible to query public and nonpublic data.
The URLs are:

* https://textgridlab.org/1.0/triplestore/textgrid-nonpublic 
* https://textgridlab.org/1.0/triplestore/textgrid-public

Only GET queries are possible.

As the triplestore in use is Sesame, the `rdf4j REST API documentation <http://docs.rdf4j.org/rest-api/#_repository_queries>`_ can be consulted.

It is possible to specify the data format to be returned with content negotiation, a list of content types is availabe in the `rdf4j documentation <http://docs.rdf4j.org/rest-api/#_content_types>`_

Example 1
---------

List all subitems of `"Alice im Wunderland" <https://textgridrep.org/textgrid:kv2r.0>`_,
and return results in JSON.

Query::

    curl -G https://textgridlab.org/1.0/triplestore/textgrid-public \
    --header 'accept: application/json' \
    --data-urlencode 'query=
    PREFIX ore: <http://www.openarchives.org/ore/terms/>
    PREFIX tg: <http://textgrid.info/relation-ns#>

    SELECT * WHERE {
      <textgrid:kv2r.0> (ore:aggregates|ore:aggregates/tg:isBaseUriOf)* ?subitem
    }'

Response::

    {
      "head" : {
        "vars" : [ "subitem" ]
      },
      "results" : {
        "bindings" : [ {
          "subitem" : {
            "type" : "uri",
            "value" : "textgrid:kv2r.0"
          }
        }, {
          "subitem" : {
            "type" : "uri",
            "value" : "textgrid:kv2q.0"
          }
        } ]
      }
    }


Example 2
---------

Construct query, list all properties, request results with format turtle.

Query::

    curl -G https://dev.textgridlab.org/1.0/triplestore/textgrid-public \
    --header 'accept: text/turtle' \
    --data-urlencode 'query=
    PREFIX tg: <http://textgrid.info/relation-ns#>
    PREFIX dc: <http://purl.org/dc/elements/1.1/>
    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX exif: <http://www.w3.org/2003/12/exif/ns#>
    PREFIX ore: <http://www.openarchives.org/ore/terms/>

    CONSTRUCT { 
      <https://textgrid.de/triplestore> <https://textgrid.de/hasTerms> ?p 
    } WHERE { 
      SELECT DISTINCT ?p WHERE 
      { ?s ?p ?o }
    }'

Response::

    @prefix tg: <http://textgrid.info/relation-ns#> .
    @prefix dc: <http://purl.org/dc/elements/1.1/> .
    @prefix dcterms: <http://purl.org/dc/terms/> .
    @prefix exif: <http://www.w3.org/2003/12/exif/ns#> .
    @prefix ore: <http://www.openarchives.org/ore/terms/> .
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix sesame: <http://www.openrdf.org/schema/sesame#> .
    @prefix owl: <http://www.w3.org/2002/07/owl#> .
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
    @prefix fn: <http://www.w3.org/2005/xpath-functions#> .

    <https://textgrid.de/triplestore> <https://textgrid.de/hasTerms> 
      tg:superType , 
      rdfs:label , 
      rdf:type , 
      tg:inProject , 
      tg:isBaseUriOf , 
      tg:notInElasticsearch , 
      dc:format , 
      owl:sameAs , 
      tg:hasPid , 
      tg:isDeleted , 
      ore:aggregates , 
      tg:isEditionOf , 
      tg:isNearlyPublished , 
      tg:hasSchema , 
      tg:hasAdaptor , 
      tg:rootElementLocalPart , 
      tg:rootElementNamespace , 
      exif:yResolution , 
      exif:xResolution , 
      exif:height , 
      exif:width , 
      tg:isDerivedFrom , 
      dc:creator , 
      dcterms:license , 
      tg:depiction .

